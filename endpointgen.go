package main

import "github.com/dave/jennifer/jen"

func (b *Builder) GenerateEndpoint(endpoint Endpoint) error {
	err := b.generateCreateURL()
	if err != nil {
		return err
	}

	return nil
}

func (b *Builder) generateCreateURL() error {
	b.file.Func().Params(
		jen.Id("c").Id("*Client"),
	).Id("createURL").Params(
		jen.Id("endpoint").String(),
	).String().Block(
		jen.Return(jen.Id("c").Dot("rootURL").Op("+").Lit("/").Op("+").Id("endpoint")),
	)
	return nil
}
