package main

import (
	"fmt"
	"testing"
)

func TestCreateURLGen(t *testing.T) {
	builder := NewBuilder()
	err := builder.generateCreateURL()
	if err != nil {
		t.Errorf("Error generating createURL: %v", err)
	}

	expected := stripTabs(`package main

	func (c *Client) createURL(endpoint string) string {
		return c.rootURL + "/" + endpoint
	}
	`)

	text := stripTabs(fmt.Sprintf("%#v", builder.file))
	if text != expected {
		t.Errorf("Result:\n%#v != expected:\n%#v", text, expected)
	}
}

func TestEndpointGen(t *testing.T) {
	t.Skip("Skipping")

	endpoint := Endpoint{
		Name:    "records",
		Url:     "/records",
		Method:  "GET",
		Returns: "Suggestion",
	}

	expected := stripTabs(`func FetchApiRecords() (*Suggestion, error) {
	}
	`)

	builder := NewBuilder()
	err := builder.GenerateEndpoint(endpoint)
	if err != nil {
		t.Errorf("Error generating endpoints: %v", err)
	}

	text := stripTabs(fmt.Sprintf("%#v", builder.file))
	if text != expected {
		t.Errorf("Result:\n%s != expected:\n%s", text, expected)
	}
}
